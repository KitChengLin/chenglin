import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home'
import Contact from '@/components/Contact'
import Profile from '@/components/Profile'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes:[
     {
        path:'/',
        name:'Home',
        component: Home
     },
     {
        path:'/Home',
        name:'Home',
        component: Home
     },
     {
         path:'/Contact',
         name:'Contact',
         component: Contact
     },
     {
         path:'/Profile',
         name:'Profile',
         component: Profile
     }
    ]
})